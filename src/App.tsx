import React from "react";
import { Layout } from "antd";
import styles from "./App.module.css";
import { Folder } from "./components/Folder";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import { ImageFolder } from "./components/ImageFolder";
import { Top } from "./components/Top";

function App() {
  console.log(styles);
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Layout.Header className={styles.header}>
        Kenseikai Video CMS
      </Layout.Header>
      <Layout.Content>
        <Router>
          <Switch>
            <Route path="/" exact component={Top} />
            <Route path="/vimeo/:folder_id?" component={Folder} />
            <Route path="/photo/:child_folder?" component={ImageFolder} />
          </Switch>
        </Router>
      </Layout.Content>
      <Layout.Footer className={styles.footer}>
        maintained by Nobuyuki Horiuchi
      </Layout.Footer>
    </Layout>
  );
}

export default App;
