import { Card, Col, Layout, Row, Image, Spin } from "antd";
import React from "react";
import {
  Route,
  Switch,
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import { useAsync } from "react-use";
import {
  FolderOpenOutlined,
  FolderViewOutlined,
  LeftCircleOutlined,
} from "@ant-design/icons";
import { Api } from "./Api";
import { getChildFolders, ImageApi } from "./ImageApi";
import { Image as ImageType } from "../generated-image-client";

export const ImageFolder: React.FC = (props) => {
  let { url } = useRouteMatch();
  const location = useLocation();
  console.log("url", url);
  return (
    <Switch>
      <Route path={`${url}/:sub_folder`}>
        {/* <div style={{ border: "1px solid black", padding: "10px" }}> */}
        <ImageFolder />
        {/* </div> */}
      </Route>
      <Route path="/">
        <FolderContent
          folder_path={location.pathname
            .replace("/photo", "/")
            .replace("//", "/")}
        />
      </Route>
    </Switch>
  );
};

const FolderContent: React.FC<{ folder_path: string }> = (props) => {
  const { folder_path } = props;
  console.log("FolderContent folder_path", folder_path);
  const items = useAsync(async () => {
    const folders = await getChildFolders(folder_path);
    // console.log("folders", folders)
    const images = await (await ImageApi().imageViewerImages(folder_path)).data;
    console.log(folder_path, folders, images)
    return [...folders, ...images];
  }, [folder_path]);
  return (
    // <div style={{ border: "1px solid red", padding: "10px" }}>
    <Layout>
      <Row gutter={[16, 16]}>
        {folder_path ? (
          <Col span={8}>
            <ParentFolderCell />
          </Col>
        ) : null}
        {items.loading ? (
          <Col span={8}>
            <LoadingCell />
          </Col>
        ) : null}
        {items.value?.map((item) => (
          <Col
            span={8}
            key={typeof item === "string" ? item : item.original_url}
          >
            <Cell item={item} />
          </Col>
        ))}
      </Row>
    </Layout>
    // </div>
  );
};

const Cell: React.FC<{ item: ImageType | string }> = (props) => {
  const { item } = props;
  if (typeof item === "string") {
    return <FolderCell item={item} />;
  } else {
    return <ImageCell item={item} />;
  }
};

const ImageCell: React.FC<{ item: ImageType }> = (props) => {
  const { item } = props;
  return (
    <Card hoverable title={item.name} bodyStyle={{ padding: 0 }}>
      <div>
        <Image
          src={item.thumbnail_url}
          preview={{
            src: item.original_url,
          }}
        />
      </div>
    </Card>
  );
};

const FolderCell: React.FC<{ item: string }> = (props) => {
  const { item } = props;
  const name = item.split("/").pop();
  const history = useHistory();
  const location = useLocation();
  return (
    <Card
      hoverable
      title={name}
      onClick={() => {
        console.log("push", location.pathname, name);
        history.push(`${location.pathname}/${name}`);
      }}
    >
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <FolderOpenOutlined />
      </div>
    </Card>
  );
};

const ParentFolderCell: React.FC = (props) => {
  const history = useHistory();
  const location = useLocation();
  return (
    <Card
      hoverable
      title={"../"}
      onClick={() => {
        history.push(location.pathname.split('/').slice(0, -1).join('/'))
      }}
    >
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <LeftCircleOutlined />
      </div>
    </Card>
  );
};

const LoadingCell: React.FC = () => {
  return (
    <Card title={"読み込み中"}>
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <Spin />
      </div>
    </Card>
  );
};
