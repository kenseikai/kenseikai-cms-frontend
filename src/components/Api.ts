import { Configuration, DefaultApiFactory } from "../generated-client";

export function Api() {
  if (window.location.href.indexOf("localhost") !== -1) {
    return DefaultApiFactory(
      new Configuration({
        basePath: "http://localhost:3000",
      })
    );
  } else {
    return DefaultApiFactory(
      new Configuration({
        basePath: `https://api.${window.location.host}`,
        username: "kenseikai",
        password: "QTgxQ0UzNzA",
      })
    );
  }
}
