import { Button, Layout } from "antd";
import React from "react";
import { Link } from "react-router-dom";

export const Top: React.VFC = () => {
  return (
    <Layout>
      <ul>
        <li>
          <Link to="/photo">画像</Link>
        </li>
        <li>
          <Link to="/vimeo">動画</Link>
        </li>
      </ul>
      <p>
        閲覧に関して、利用者は無料です。Vimeoのサービスで課金への導線がある場合がありますが、申し込まないでください
      </p>
    </Layout>
  );
};
