import {
  Configuration,
  ImageViewerApiFactory,
} from "../generated-image-client";

export function ImageApi() {
  return ImageViewerApiFactory(
    new Configuration({
      basePath: `https://xfzqmlmzue.execute-api.us-east-1.amazonaws.com`,
    })
  );
}

let foldersPromise: Promise<string[]>;
export async function getChildFolders(parent: string) {
  // console.log('getChildFolders', parent)
  if (!foldersPromise) {
    foldersPromise = (async () => {
      return (await ImageApi().imageViewerFolders()).data;
    })();
  }
  const folders = await foldersPromise;
  // console.log(folders)
  return folders
    .filter((f) => f.startsWith(`${parent}/`.replace('//', '/')) && f.length > parent.length)
    .filter((f) => f.substring(parent.length + 1).split("/").length <= 1);
}
