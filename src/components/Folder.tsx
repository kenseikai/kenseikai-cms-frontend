import { Card, Col, Layout, Row, Image, Spin } from "antd";
import React from "react";
import {
  Route,
  Switch,
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import { useAsync } from "react-use";
import {
  Configuration,
  DefaultApiFactory,
  FolderType,
  VideoType,
} from "../generated-client";

import {
  FolderOpenOutlined,
  FolderViewOutlined,
  LeftCircleOutlined,
} from "@ant-design/icons";
import { Api } from "./Api";

export const Folder: React.FC = (props) => {
  let { url } = useRouteMatch();
  const { folder_id } = useParams<{ folder_id?: string }>();
  // console.log("folder_id", folder_id);
  return (
    <Switch>
      <Route path={`${url}/:folder_id`}>
        {/* <div style={{ border: "1px solid black", padding: "10px" }}> */}
        <Folder />
        {/* </div> */}
      </Route>
      <Route path="/">
        <FolderContent folder_id={folder_id} />
      </Route>
    </Switch>
  );
};

const FolderContent: React.FC<{ folder_id?: string }> = (props) => {
  const { folder_id } = props;
  console.log("folder_id", folder_id);
  const items = useAsync(async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    if (!folder_id) {
      const result = await Api().vimeoControllerGetRootFolder();
      return result.data.items;
    } else {
      const result = await Api().vimeoControllerGetFolder(folder_id);
      return result.data.items;
    }
  }, [folder_id]);
  return (
    // <div style={{ border: "1px solid red", padding: "10px" }}>
    <Layout>
      <Row gutter={[16, 16]}>
        {folder_id ? (
          <Col span={8}>
            <ParentFolderCell />
          </Col>
        ) : null}
        {items.loading ? (
          <Col span={8}>
            <LoadingCell />
          </Col>
        ) : null}
        {items.value?.map((item) => (
          <Col span={8} key={item.uri}>
            <Cell item={item} />
          </Col>
        ))}
      </Row>
    </Layout>
    // </div>
  );
};

const Cell: React.FC<{ item: FolderType | VideoType }> = (props) => {
  const { item } = props;
  if (item.type === "video") {
    return <VideoCell item={item as VideoType} />;
  } else if (item.type === "folder") {
    return <FolderCell item={item as FolderType} />;
  } else {
    console.warn("unknown type", item.type);
    return <></>;
  }
};

const VideoCell: React.FC<{ item: VideoType }> = (props) => {
  const { item } = props;
  return (
    <Card
      hoverable
      title={item.name}
      onClick={() => {
        window.open(`${item.link}`, "_blank");
      }}
      bodyStyle={{ padding: 0 }}
    >
      <div>
        <Image src={item.picture_link} preview={false} />
      </div>
    </Card>
  );
};

const FolderCell: React.FC<{ item: FolderType }> = (props) => {
  const { item } = props;
  const history = useHistory();
  const location = useLocation();
  return (
    <Card
      hoverable
      title={item.name}
      onClick={() => {
        const folder_id = item.uri.match(/\/users\/\d+?\/projects\/(\d+)/)![1];
        console.log("push", location.pathname, folder_id);
        history.push(`${location.pathname}/${folder_id}`);
      }}
    >
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <FolderOpenOutlined />
      </div>
    </Card>
  );
};

const ParentFolderCell: React.FC = (props) => {
  const history = useHistory();
  const location = useLocation();
  return (
    <Card
      hoverable
      title={"../"}
      onClick={() => {
        const match = location.pathname.match(/([\w/]+)\/\d+/);
        if (match) {
          history.push(match![1]);
        }
      }}
    >
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <LeftCircleOutlined />
      </div>
    </Card>
  );
};

const LoadingCell: React.FC = () => {
  return (
    <Card title={"読み込み中"}>
      <div style={{ textAlign: "center", fontSize: 36, lineHeight: 1.5 }}>
        <Spin />
      </div>
    </Card>
  );
};
