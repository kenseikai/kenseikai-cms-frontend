/* tslint:disable */
/* eslint-disable */
/**
 * Image Viewer Service
 * HTTP service for viewing image files hosted on S3
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import { Configuration } from './configuration';
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
// Some imports not used depending on template conditions
// @ts-ignore
import { DUMMY_BASE_URL, assertParamExists, setApiKeyToObject, setBasicAuthToObject, setBearerAuthToObject, setOAuthToObject, setSearchParams, serializeDataIfNeeded, toPathString, createRequestFunction } from './common';
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from './base';

/**
 * 
 * @export
 * @interface Image
 */
export interface Image {
    /**
     * name
     * @type {string}
     * @memberof Image
     */
    name?: string;
    /**
     * signed original image url
     * @type {string}
     * @memberof Image
     */
    original_url?: string;
    /**
     * signed thumbnail image url
     * @type {string}
     * @memberof Image
     */
    thumbnail_url?: string;
}

/**
 * ImageViewerApi - axios parameter creator
 * @export
 */
export const ImageViewerApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @summary folders image-viewer
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        imageViewerFolders: async (options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/folders`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter, options.query);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary images image-viewer
         * @param {string} [folder] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        imageViewerImages: async (folder?: string, options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/images`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (folder !== undefined) {
                localVarQueryParameter['folder'] = folder;
            }


    
            setSearchParams(localVarUrlObj, localVarQueryParameter, options.query);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * ImageViewerApi - functional programming interface
 * @export
 */
export const ImageViewerApiFp = function(configuration?: Configuration) {
    const localVarAxiosParamCreator = ImageViewerApiAxiosParamCreator(configuration)
    return {
        /**
         * 
         * @summary folders image-viewer
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async imageViewerFolders(options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Array<string>>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.imageViewerFolders(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @summary images image-viewer
         * @param {string} [folder] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async imageViewerImages(folder?: string, options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Array<Image>>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.imageViewerImages(folder, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
    }
};

/**
 * ImageViewerApi - factory interface
 * @export
 */
export const ImageViewerApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    const localVarFp = ImageViewerApiFp(configuration)
    return {
        /**
         * 
         * @summary folders image-viewer
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        imageViewerFolders(options?: any): AxiosPromise<Array<string>> {
            return localVarFp.imageViewerFolders(options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary images image-viewer
         * @param {string} [folder] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        imageViewerImages(folder?: string, options?: any): AxiosPromise<Array<Image>> {
            return localVarFp.imageViewerImages(folder, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * ImageViewerApi - object-oriented interface
 * @export
 * @class ImageViewerApi
 * @extends {BaseAPI}
 */
export class ImageViewerApi extends BaseAPI {
    /**
     * 
     * @summary folders image-viewer
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof ImageViewerApi
     */
    public imageViewerFolders(options?: any) {
        return ImageViewerApiFp(this.configuration).imageViewerFolders(options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary images image-viewer
     * @param {string} [folder] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof ImageViewerApi
     */
    public imageViewerImages(folder?: string, options?: any) {
        return ImageViewerApiFp(this.configuration).imageViewerImages(folder, options).then((request) => request(this.axios, this.basePath));
    }
}


